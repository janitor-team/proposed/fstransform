fstransform (0.9.4-1) unstable; urgency=medium

  * Package new upstream release 0.9.4.
    + Drop patch that was merged upstream.
    + Update debian/copyright accordingly.
  * Adopt the Salsa CI team pipeline.
  * Update debian/watch and debian/control to point to the new repository on
    GitHub.
  * Bump Standards-Version to 4.4.1 (no changes required).

 -- Giovanni Mascellani <gio@debian.org>  Fri, 03 Jan 2020 16:33:09 +0100

fstransform (0.9.3-4) unstable; urgency=medium

  * Backport upstream patch that properly fix a data corruption buf (closes:
    #925957).
  * Remove warning message, which is now useless.

 -- Giovanni Mascellani <gio@debian.org>  Thu, 04 Apr 2019 08:50:30 +0200

fstransform (0.9.3-3) unstable; urgency=medium

  [ Giovanni Mascellani ]
  * Update Vcs-* after migration to salsa.
  * Display a more explicit warning message about the risk of data corruption
    when executing with little available memory (closes: #925957).

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/control: Deprecating priority extra as per policy 4.0.1
  * d/watch: Use https protocol

 -- Giovanni Mascellani <gio@debian.org>  Sat, 30 Mar 2019 11:24:21 +0100

fstransform (0.9.3-2) unstable; urgency=low

  * Bump debian/compat to 9 in order to correctly use default build flags
    and enable hardening flags.
  * Bump Standards-Version to 3.9.8 (no changes required).
  * Fix a few smaller details.

 -- Giovanni Mascellani <gio@debian.org>  Fri, 29 Apr 2016 11:34:28 +0200

fstransform (0.9.3-1) unstable; urgency=low

  * New upstream release.
  * Slight description rewording.
  * Bump Standards-Version to 3.9.3 (no change required).

 -- Giovanni Mascellani <gio@debian.org>  Fri, 13 Apr 2012 17:54:14 +0200

fstransform (0.9.2-1) unstable; urgency=low

  * Initial release (closes: #661266).

 -- Giovanni Mascellani <gio@debian.org>  Fri, 09 Mar 2012 23:03:39 +0100
